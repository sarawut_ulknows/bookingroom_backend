
const jwt = require('jsonwebtoken')

//! gent Token
const generateAccessToken = function (user) {
  return jwt.sign(user, process.env.TOKEN_SECRET, { expiresIn: '1d' })
}

module.exports = { generateAccessToken }
