
const authorizeMiddleware = function (roles) {
  return function (req, res, next) {
    //! เช็คว่าเป็น adminไหม

    for (let i = 0; i < roles.length; i++) {
      // ? เจอ

      if (req.user.roles.indexOf(roles[i]) >= 0) {
        next()
        return
      }
    }
    res.sendStatus(401)
  }
}

module.exports = { authorizeMiddleware }
