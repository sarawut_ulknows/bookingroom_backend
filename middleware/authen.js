// const User = require('../models/User')
const jwt = require('jsonwebtoken')
const User = require('../models/User')
/* ------------- authen a token [middleware] ------------ */
const authenMiddleware = function (req, res, next) {
  // ! check จาก header
  const authHeader = req.headers.authorization
  // มี authenHeader หรือป่าว && split แล้วเอาตัวที่ 1
  const token = authHeader && authHeader.split(' ')[1]
  // ถ้า token == null
  if (token === null) return res.sendStatus(401)
  jwt.verify(token, process.env.TOKEN_SECRET, async function (err, user) {
    console.log(err)
    if (err) return res.sendStatus(403)
    // req.user = user
    const currentUser = await User.findById(user._id).exec()
    req.user = currentUser
    next()
  })
}
module.exports = { authenMiddleware }
