const mongoose = require('mongoose')
const Approve = require('../models/Approve')
const User = require('../models/User')
mongoose.connect('mongodb://localhost:27017/team')

const approveRun = async function () {
  await clearApprove()
  const approver = await User.find({})

  const ap1 = new Approve({
    approver: approver[1],
    approve_date: new Date('2022-01-03 10:05'),
    approve_status: true

  })
  const ap2 = new Approve({
    approver: approver[2]

  })
  const ap3 = new Approve({
    approver: approver[2]

  })
  const ap4 = new Approve({
    approver: approver[3]

  })
  const ap5 = new Approve({
    approver: approver[2]

  })
  const ap6 = new Approve({
    approver: approver[3]

  })
  const ap7 = new Approve({
    approver: approver[1],
    approve_date: new Date('2022-01-03 10:10'),
    approve_status: false

  })

  //! สร้างข้อมูล และบันทึกลง collection Approve
  ap1.save()
  ap2.save()
  ap3.save()
  ap4.save()
  ap5.save()
  ap6.save()
  ap7.save()
}
//! function clear DB
async function clearApprove () {
  await Approve.deleteMany({})
}

// approveRun().then(() => console.log('Finish'))
module.exports = approveRun
