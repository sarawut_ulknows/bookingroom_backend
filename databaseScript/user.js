const mongoose = require('mongoose')
const User = require('../models/User')
const Institution = require('../models/Institution')
const { ROLE } = require('../routes/ROLE')
mongoose.connect('mongodb://localhost:27017/team')

const userRun = async function () {
  await clearProduct()

  const institutions = await Institution.find({})
  console.log(institutions)

  //! สร้างข้อมูล และบันทึกลง collection Product
  const user01 = new User({
    username: 'bestza01',
    password: 'bestza01',
    name: 'Pakawat',
    surname: 'Jaroenying',
    roles: [ROLE.USER, ROLE.ADMIN],
    institution: institutions[3]
  })

  const user02 = new User({
    username: 'stangza01',
    password: 'stangza01',
    name: 'Stang',
    surname: 'Thanawat',
    roles: [ROLE.LOCAL_ADMIN],
    institution: institutions[0]
  })

  const user03 = new User({
    username: 'stunza01',
    password: 'stunza01',
    name: 'Stun',
    surname: 'Pisitpong',
    roles: [ROLE.LOCAL_ADMIN],
    institution: institutions[1]
  })
  const user04 = new User({
    username: 'palmyza01',
    password: 'palmyza01',
    name: 'Palmy',
    surname: 'Pisit',
    roles: [ROLE.LOCAL_ADMIN],
    institution: institutions[2]
  })

  await user01.save()
  await user02.save()
  await user03.save()
  await user04.save()
}
//! function clear DB
async function clearProduct () {
  await User.deleteMany({})
}

module.exports = userRun
// userRun().then(() => console.log('Finish'))
