const mongoose = require('mongoose')
const Booking = require('../models/Booking')
const User = require('../models/User')
const Room = require('../models/Room')
const Approve = require('../models/Approve')

mongoose.connect('mongodb://localhost:27017/team')

const bookingRun = async function () {
  await clearBooking()
  const users = await User.find({}).exec()
  const rooms = await Room.find({}).exec()
  const approves = await Approve.find({}).exec()

  // const ap1 = new Approve()
  const b1 = new Booking(
    {
      user: users[0],
      datetime_reserve: new Date('2022-02-25 09:30'),
      room: rooms[3],
      datetime_start: new Date('2022-02-28 10:00'),
      datetime_end: new Date('2022-02-28 12:00'),
      status: -1,
      approves: [approves[4], approves[5]]
    }
  )
  const b2 = new Booking(
    {
      user: users[2],
      datetime_reserve: new Date('2022-02-26 12:25'),
      room: rooms[2],
      datetime_start: new Date('2022-02-28 13:00'),
      datetime_end: new Date('2022-02-28 14:00'),
      status: -1,
      approves: [approves[2], approves[3]]
    }
  )
  const b3 = new Booking(
    {
      user: users[1],
      datetime_reserve: new Date('2022-02-26 14:35'),
      room: rooms[1],
      datetime_start: new Date('2022-02-28 14:00'),
      datetime_end: new Date('2022-02-28 15:00'),
      status: -1,
      approves: [approves[1]]
    }
  )
  const b4 = new Booking(
    {
      user: users[3],
      datetime_reserve: new Date('2022-02-26 10:15'),
      room: rooms[0],
      datetime_start: new Date('2022-02-29 9:00'),
      datetime_end: new Date('2022-02-29 11:00'),
      status: 1,
      approves: [approves[0]]
    }
  )
  const b5 = new Booking(
    {
      user: users[1],
      datetime_reserve: new Date('2022-02-27 12:35'),
      room: rooms[4],
      datetime_start: new Date('2022-02-29 11:00'),
      datetime_end: new Date('2022-02-29 12:00'),
      status: 0,
      approves: [approves[6]]
    }
  )
  //! สร้างข้อมูล และบันทึกลง collection Booking
  b1.save()
  b2.save()
  b3.save()
  b4.save()
  b5.save()
}
//! function clear DB
async function clearBooking () {
  await Booking.deleteMany({})
}

// bookingRun().then(() => console.log('booking Finish'))
module.exports = bookingRun
