const mongoose = require('mongoose')
const Room = require('../models/room')
const Building = require('../models/building')
const Equipment = require('../models/equipment')
const Approver = require('../models/approver')
const Institution = require('../models/Institution')
// const Booking = require('../models/booking')
mongoose.connect('mongodb://localhost:27017/team')

const roomRun = async function () {
  await clearRoom()
  const buildings = await Building.find({}).exec()
  const equipments = await Equipment.find({}).exec()
  const approveres = await Approver.find({}).exec()
  // const bookings = await Booking.find({}).exec()
  const institutions = await Institution.find({}).exec()

  const r1 = new Room({
    code: 'IF-8C01',
    capacity: 6,
    floor: 8,
    building: buildings[0],
    equipment: equipments,
    approveres: approveres[0],
    institution: institutions[1]

  })
  const r2 = new Room({

    code: 'COMP-8T01',
    capacity: 8,
    floor: 8,
    building: buildings[0],
    equipment: equipments,
    approveres: approveres[1],
    institution: institutions[2]

  })
  const r3 = new Room({
    code: 'IC-2C01',
    capacity: 8,
    floor: 2,
    building: buildings[3],
    equipment: equipments,
    approveres: approveres[2],
    institution: institutions[2]

  })
  const r4 = new Room({
    code: 'IC-3C01',
    capacity: 10,
    floor: 3,
    building: buildings[3],
    equipment: equipments,
    approveres: approveres[2],
    institution: institutions[2]

  })
  const r5 = new Room({
    code: 'UAD-2T01',
    capacity: 8,
    floor: 2,
    building: buildings[4],
    equipment: equipments,
    approveres: approveres[0],
    institution: institutions[3]

  })

  //! สร้างข้อมูล และบันทึกลง collection Room
  r1.save()
  r2.save()
  r3.save()
  r4.save()
  r5.save()
  console.log('Roomrun')
}
//! function clear DB
async function clearRoom () {
  await Room.deleteMany({})
}

// roomRun().then(() => console.log('Finish'))

module.exports = roomRun
