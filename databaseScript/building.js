const mongoose = require('mongoose')
// const Room = require('../models/room')
const Building = require('../models/building')
mongoose.connect('mongodb://localhost:27017/team')

const buildingRun = async function () {
  await clearBuilding()

  // const rooms = await Room.find({}).exec()

  const b1 = new Building({
    code: 'IF',
    name: 'Faculty of Informatics',
    floor: 10

  })
  const b2 = new Building({
    code: 'COMP',
    name: 'Computer Center',
    floor: 10

  })
  const b3 = new Building({
    code: 'MA',
    name: 'Department of Mathematics',
    floor: 10

  })
  const b4 = new Building({
    code: 'IC',
    name: 'International college',
    floor: 10

  })
  const b5 = new Building({
    code: 'UAD',
    name: 'Thamrong Buasri Building',
    floor: 10

  })

  //! สร้างข้อมูล และบันทึกลง collection Building
  b1.save()
  b2.save()
  b3.save()
  b4.save()
  b5.save()
}
//! function clear DB
async function clearBuilding () {
  await Building.deleteMany({})
}

// buildingRun().then(() => console.log('Finish'))
module.exports = buildingRun
