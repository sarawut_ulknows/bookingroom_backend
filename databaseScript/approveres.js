
const mongoose = require('mongoose')
const User = require('../models/User')
const Approver = require('../models/Approver')
const Institution = require('../models/Institution')
mongoose.connect('mongodb://localhost:27017/team')

const approveresRun = async function () {
  await clearApproveres()

  const users = await User.find({}).exec()
  const Institutions = await Institution.find().exec()
  console.log(users)

  const approver1 = new Approver({
    name: 'approver1',
    approveres: [users[1]],
    institution: Institutions[1]
  })

  const approver2 = new Approver({
    name: 'approver2',
    approveres: [users[2]],
    institution: Institutions[2]
  })

  const approver3 = new Approver({
    name: 'approver3',
    approveres: [users[2], users[3]],
    institution: Institutions[2]
  })

  const approver4 = new Approver({
    name: 'approver4',
    approveres: [users[0], users[3]],
    institution: Institutions[3]
  })

  //! สร้างข้อมูล และบันทึกลง collection Institution
  approver1.save()
  approver2.save()
  approver3.save()
  approver4.save()
}
//! function clear DB
async function clearApproveres () {
  await Approver.deleteMany({})
}

// approveresRun().then(() => console.log('Finish'))
module.exports = approveresRun
