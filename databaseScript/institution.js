const mongoose = require('mongoose')
const Institution = require('../models/Institution')
mongoose.connect('mongodb://localhost:27017/team')

const institutionRun = async function () {
  await clearInstitution()
  const facultyOfinformatics = new Institution({
    name: 'Faculty of Informatics'

  })
  const facultyOfBusiness = new Institution({
    name: 'Faculty of Business Administration'
  })
  const science = new Institution({
    name: 'Faculty of Science'

  })
  const healthScri = new Institution({
    name: 'Faculty of Health Sciences'
  })
  //! สร้างข้อมูล และบันทึกลง collection Institution
  facultyOfinformatics.save()
  facultyOfBusiness.save()
  science.save()
  healthScri.save()
}
//! function clear DB
async function clearInstitution () {
  await Institution.deleteMany({})
}

module.exports = institutionRun
