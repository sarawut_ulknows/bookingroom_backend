const roomRun = require('./room')
const institutionRun = require('./institution')
const userRun = require('./user')
const approveresRun = require('./approveres')
const equipmentRun = require('./equipment')
const buildingRun = require('./building')
const approveRun = require('./approve')
const bookingRun = require('./booking')

async function main () {
  await institutionRun()
  await userRun()
  await approveresRun()
  await equipmentRun()
  await buildingRun()
  await roomRun()
  await approveRun()
  await bookingRun()
}

main().then(() => console.log('Finish'))
