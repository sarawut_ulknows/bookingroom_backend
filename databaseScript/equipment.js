const mongoose = require('mongoose')
const Equipment = require('../models/equipment')
mongoose.connect('mongodb://localhost:27017/team')

const equipmentRun = async function () {
  await clearEquipment()

  const eq1 = new Equipment({
    name: 'microphone'
  })
  const eq2 = new Equipment({
    name: 'TV'
  })
  const eq3 = new Equipment({
    name: 'Projector'
  })
  const eq4 = new Equipment({
    name: 'Presenter'
  })
  //! สร้างข้อมูล และบันทึกลง collection Equipment
  eq1.save()
  eq2.save()
  eq3.save()
  eq4.save()
}
//! function clear DB
async function clearEquipment () {
  await Equipment.deleteMany({})
}

// equipmentRun().then(() => console.log('Finish'))
module.exports = equipmentRun
