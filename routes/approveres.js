const express = require('express')
const router = express.Router()
const Approver = require('../models/approver')

const getApproveres = async function (req, res, next) {
  try {
    const approveres = await Approver.find({}).populate('approveres institution')
    res.status(200).json(approveres)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}
const getApprover = async function (req, res, next) {
  try {
    const approveres = await Approver.findOne({ _id: req.params.id })
    res.status(200).json(approveres)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}

const addApprover = async function (req, res, next) {
  const newApprover = new Approver(
    {
      name: req.body.name,
      approveres: req.body.approveres,
      institution: req.body.institution
    }
  )

  try {
    await newApprover.save()
    res.status(201).json(newApprover)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}
const deleteApprover = async function (req, res, next) {
  try {
    const deletedItem = await Approver.findOneAndDelete({ _id: req.params.id })

    res.status(200).json(deletedItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}
const editApprover = async function (req, res, next) {
  const id = req.params.id
  try {
    const previousItem = await Approver.findById(id).exec()
    previousItem.name = req.body.name
    previousItem.approveres = req.body.approveres
    previousItem.institution = req.body.institution
    await previousItem.save()
    res.status(200).send(previousItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}

/* ------------------------------------------------------ */
router.get('/', getApproveres)
router.get('/:id', getApprover)
router.post('/', addApprover)
router.delete('/:id', deleteApprover)
router.put('/:id', editApprover)

/* ----------------------- TESTED ----------------------- */

module.exports = router
