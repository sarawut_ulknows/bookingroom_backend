const express = require('express')
const router = express.Router()
const Building = require('../models/Building')

/* ------------------------- get ------------------------ */
const getBuildings = async function (req, res, next) {
  try {
    const Buildings = await Building.find({}).exec()

    res.status(200).json(Buildings)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */
/* ------------------------- get :id ------------------------ */
const getBuilding = async function (req, res, next) {
  try {
    const Buildings = await Building.findOne({ _id: req.params.id }).exec()

    res.status(200).json(Buildings)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */

/* ------------------------ post ------------------------ */
const addBuilding = async function (req, res, next) {
  const newBuilding = new Building(
    {
      name: req.body.name,
      code: req.body.code,
      floor: req.body.floor
    }
  )

  try {
    await newBuilding.save()
    res.status(201).json(newBuilding)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}

/* ----------------------- delete ----------------------- */
const deleteBuilding = async function (req, res, next) {
  try {
    const deletedItem = await Building.findOneAndDelete({ _id: req.params.id })
    res.status(200).json(deletedItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */

/* ------------------------- PUT ------------------------ */
const editBuilding = async function (req, res, next) {
  const id = req.params.id
  try {
    const previousItem = await Building.findById(id).exec()
    previousItem.name = req.body.name
    previousItem.code = req.body.code
    previousItem.floor = req.body.floor
    await previousItem.save()
    res.status(200).send(previousItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}

/* ------------------------------------------------------ */

/* ------------------------------------------------------ */
router.get('/:id', getBuilding)
router.get('/', getBuildings)
router.post('/', addBuilding)
router.delete('/:id', deleteBuilding)
router.put('/:id', editBuilding)

/* ----------------------- TESTED ----------------------- */

module.exports = router
