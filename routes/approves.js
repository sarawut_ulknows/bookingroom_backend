const express = require('express')
const router = express.Router()
const Approve = require('../models/Approve')
const Booking = require('../models/Booking')

const getApproves = async function (req, res, next) {
  try {
    const Approves = await Approve.find({}).populate('approver')
    res.status(200).json(Approves)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}
const getApprove = async function (req, res, next) {
  try {
    const approve = await Approve.findOne({ approver: req.params.id })
    res.status(200).json(approve)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}

const addApprover = async function (req, res, next) {
  const newApprover = new Approve(
    {
      approver: req.body.approver,
      approve_date: req.body.approve_date,
      approve_status: req.body.approve_status
    }
  )

  try {
    await newApprover.save()
    res.status(201).json(newApprover)
  } catch (e) {
    console.log(e)
    return res.status(500).send({
      massage: e.massage
    })
  }
}
const deleteApprover = async function (req, res, next) {
  try {
    const deletedItem = await Approve.findOneAndDelete({ _id: req.params.id })

    res.status(200).json(deletedItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}

const editApprove = async function (req, res, next) {
  const id = req.params.id
  try {
    const previousItem = await Approve.findById(id).exec()
    console.log('previousItem', previousItem)
    previousItem.approver = req.body.approver
    previousItem.approve_date = req.body.approve_date
    previousItem.approve_status = req.body.approve_status
    await previousItem.save()

    // เปลื่ยน status booking
    const booking = await Booking.findOne({ approves: { $in: req.params.id } }).populate('approves')
    // เช็คสถานะ approve แต่ละตัว
    for (let i = 0; i < booking.approves.length; i++) {
      const approve = booking.approves[i]
      console.log('approve ', i, '=', approve.approve_status)
      // console.log(typeof (approve.approve_status))
      // console.log(approve)
      // ถ้ามีผู้อนุมัติกดปฎิเสธ

      if (approve.approve_status === false) {
        booking.status = -1
        await booking.save()
        break
      } else if (approve.approve_status === null) {
        booking.status = 0
        await booking.save()
        break
      } else if (approve.approve_status === true && booking.approves.lenght !== (i - 1)) {
        booking.status = 1
        await booking.save()
      }
    }
    res.status(200).send(previousItem)
    // หาbookings เพื่อตั้งค่า status ใหม่
    // for()
  } catch (e) {
    // console.log('e.massage', e.massage)
    res.status(500).send({
      massage: e.massage
    })
  }
}

/* ------------------------------------------------------ */
router.get('/', getApproves)
router.get('/:id', getApprove)
router.put('/:id', editApprove)
router.post('/', addApprover)
router.delete('/:id', deleteApprover)

/* ----------------------- TESTED ----------------------- */

module.exports = router
