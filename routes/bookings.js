const express = require('express')
const router = express.Router()
const Booking = require('../models/Booking')
const Room = require('../models/Room')
const Approver = require('../models/Approver')
const Approve = require('../models/Approve')

const getBookings = async function (req, res, next) {
  try {
    const Bookings = await Booking.find({})
    res.status(200).json(Bookings)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}
const getBooking = async function (req, res, next) {
  try {
    const booking = await Booking.findOne({ _id: req.params.id }).populate('approves')
    res.status(200).json(booking)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}
const getApprove = async function (req, res, next) {
  try {
    const booking = await Booking.findOne({ approves: { $in: req.params.id } }).populate('user room')
    res.status(200).json(booking)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}

const addBooking = async function (req, res, next) {
  // const user = User.findOne({ _id: req.body.user })
  console.log(req.body.user)

  const newBooking = new Booking(
    {
      user: req.body.user,
      datetime_reserve: req.body.datetime_reserve,
      room: req.body.room,
      datetime_start: req.body.datetime_start,
      datetime_end: req.body.datetime_end,
      status: 0
    }
  )

  try {
    // หาว่ามีลำดับการพิจารณาอย่างไรในห้องนั้น  เพื่อไปสร้าง approve
    const room = await Room.findOne({ _id: newBooking.room._id }, 'approveres')

    // หาในตาราง approver ว่ามีใครเป็นผู้อนุมัติบ้าง
    const approveres = await Approver.findOne({ _id: room.approveres._id }, 'approveres')

    // เพิ่มเข้าตาราง approve
    for (let i = 0; i < approveres.approveres.length; i++) {
      const approve = new Approve({
        approver: approveres.approveres[i],
        approve_date: null,
        approve_status: null
      })

      newBooking.approves.push(approve)
      await approve.save()
    }

    await newBooking.save()
    res.status(201).json(newBooking)
  } catch (e) {
    console.log(e)
    return res.status(500).send({
      massage: e.massage
    })
  }
}
const deleteBooking = async function (req, res, next) {
  try {
    const deletedItem = await Booking.findOneAndDelete({ _id: req.params.id })

    res.status(200).json(deletedItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}

/* ------------------------------------------------------ */
router.get('/', getBookings)
router.get('/:id', getBooking)
router.get('/approve/:id', getApprove)
router.post('/', addBooking)
router.delete('/:id', deleteBooking)

/* ----------------------- TESTED ----------------------- */

module.exports = router
