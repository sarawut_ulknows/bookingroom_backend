const express = require('express')
const router = express.Router()
const Equipment = require('../models/Equipment')

/* ------------------------- get ------------------------ */
const getEquipments = async function (req, res, next) {
  try {
    const Equipments = await Equipment.find({}).exec()

    res.status(200).json(Equipments)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */
/* ------------------------- get :id ------------------------ */
const getEquipment = async function (req, res, next) {
  try {
    const Equipments = await Equipment.findOne({ _id: req.params.id }).exec()

    res.status(200).json(Equipments)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */

/* ------------------------ post ------------------------ */
const addEquipment = async function (req, res, next) {
  const newEquipment = new Equipment(
    {
      name: req.body.name
    }
  )

  try {
    await newEquipment.save()
    res.status(201).json(newEquipment)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}

/* ----------------------- delete ----------------------- */
const deleteEquipment = async function (req, res, next) {
  try {
    const deletedItem = await Equipment.findOneAndDelete({ _id: req.params.id })
    res.status(200).json(deletedItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */

/* ------------------------- PUT ------------------------ */
const editEquipment = async function (req, res, next) {
  const id = req.params.id
  try {
    const previousItem = await Equipment.findById(id).exec()
    previousItem.name = req.body.name
    await previousItem.save()
    res.status(200).send(previousItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}

/* ------------------------------------------------------ */

/* ------------------------------------------------------ */
router.get('/:id', getEquipment)
router.get('/', getEquipments)
router.post('/', addEquipment)
router.delete('/:id', deleteEquipment)
router.put('/:id', editEquipment)

/* ----------------------- TESTED ----------------------- */

module.exports = router
