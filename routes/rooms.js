const express = require('express')
const router = express.Router()
const Room = require('../models/Room')
const Institution = require('../models/Institution')
const Building = require('../models/Building')
const Approver = require('../models/Approver')

/* ------------------------- get ------------------------ */
const getRooms = async function (req, res, next) {
  try {
    const Rooms = await Room.find({}).populate('institution equipment building approveres').exec()

    res.status(200).json(Rooms)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */
/* ------------------------- get :id ------------------------ */
const getRoom = async function (req, res, next) {
  try {
    const Rooms = await Room.findOne({ _id: req.params.id }).exec()

    res.status(200).json(Rooms)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */

/* ------------------------ post ------------------------ */
const addRoom = async function (req, res, next) {
  console.log(req.body.institution)
  // แปลง id -> object ของ Institution
  const institution = await Institution.findOne({ _id: req.body.institution })
  // แปลง id -> object ของ Building
  const building = await Building.findOne({ _id: req.body.building })
  // แปลง id -> object ของ approveres
  const approveres = await Approver.findOne({ _id: req.body.approveres })

  const newRoom = new Room(
    {
      code: req.body.code,
      capacity: req.body.capacity,
      floor: req.body.floor,
      building: building,
      equipment: req.body.equipment,
      approveres: approveres,
      institution: institution
    }
  )

  try {
    console.log(newRoom)
    await newRoom.save()
    res.status(201).json(newRoom)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}

/* ----------------------- delete ----------------------- */
const deleteRoom = async function (req, res, next) {
  try {
    const deletedItem = await Room.findOneAndDelete({ _id: req.params.id })
    res.status(200).json(deletedItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */

/* ------------------------- PUT ------------------------ */
const editRoom = async function (req, res, next) {
  const id = req.params.id
  try {
    const previousItem = await Room.findById(id).exec()
    previousItem.code = req.body.code
    previousItem.capacity = req.body.capacity
    previousItem.floor = req.body.floor
    previousItem.building = req.body.building
    previousItem.equipment = req.body.equipment
    previousItem.approveres = req.body.approveres
    previousItem.institution = req.body.institution
    await previousItem.save()
    res.status(200).send(previousItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}

/* ------------------------------------------------------ */

/* ------------------------------------------------------ */
router.get('/:id', getRoom)
router.get('/', getRooms)
router.post('/', addRoom)
router.delete('/:id', deleteRoom)
router.put('/:id', editRoom)

/* ----------------------- TESTED ----------------------- */

module.exports = router
