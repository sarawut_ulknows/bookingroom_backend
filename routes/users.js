const express = require('express')
const router = express.Router()
const User = require('../models/User')

/* ------------------------- get ------------------------ */
const getUsers = async function (req, res, next) {
  try {
    const users = await User.find({}).populate('institution')

    res.status(200).json(users)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */
/* ------------------------- get :id ------------------------ */
const getUser = async function (req, res, next) {
  try {
    const users = await User.findOne({ _id: req.params.id }).exec()

    res.status(200).json(users)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */

/* ------------------------ post ------------------------ */
const addUser = async function (req, res, next) {
  const newUser = new User(
    {
      username: req.body.username,
      password: req.body.password,
      name: req.body.name,
      surname: req.body.surname,
      institution: req.body.institution,
      roles: req.body.roles
    }
  )
  /* ------------------------ debug lenght from req.body.roles ----------------------- */
  // console.log(req.body.roles.length)

  // for (let i = 0; i < req.body.roles.length; i++) {
  //   newUser.roles.push(req.body.roles[i])
  // }

  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}

/* ----------------------- delete ----------------------- */
const deleteUser = async function (req, res, next) {
  try {
    const deletedItem = await User.findOneAndDelete({ _id: req.params.id })
    res.status(200).json(deletedItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */

/* ------------------------- PUT ------------------------ */
const editUser = async function (req, res, next) {
  const id = req.params.id
  try {
    /* ------------------------------------------------------ */
    const previousItem = await User.findById(id).exec()
    previousItem.username = req.body.username
    previousItem.password = req.body.password
    previousItem.name = req.body.name
    previousItem.surname = req.body.surname
    previousItem.roles = req.body.roles
    previousItem.institution = req.body.institution
    await previousItem.save()
    res.status(200).send(previousItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}

/* ------------------------------------------------------ */

/* ----------------------- TESTED ----------------------- */

/* ------------------------------------------------------ */
router.get('/', getUsers)
router.get('/:id', getUser)
router.post('/', addUser)
router.delete('/:id', deleteUser)
router.put('/:id', editUser)

module.exports = router
