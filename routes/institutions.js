const express = require('express')
const router = express.Router()
const Institution = require('../models/Institution')

/* ------------------------- get ------------------------ */
const getInstitutions = async function (req, res, next) {
  try {
    const institutions = await Institution.find({}).exec()

    res.status(200).json(institutions)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */
/* ------------------------- get :id ------------------------ */
const getInstitution = async function (req, res, next) {
  try {
    const institutions = await Institution.findOne({ _id: req.params.id }).exec()

    res.status(200).json(institutions)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */

/* ------------------------ post ------------------------ */
const addInstitution = async function (req, res, next) {
  const newInstitution = new Institution(
    {
      name: req.body.name
    }
  )

  try {
    await newInstitution.save()
    res.status(201).json(newInstitution)
  } catch (e) {
    return res.status(500).send({
      massage: e.massage
    })
  }
}

/* ----------------------- delete ----------------------- */
const deleteInstitution = async function (req, res, next) {
  try {
    const deletedItem = await Institution.findOneAndDelete({ _id: req.params.id })
    res.status(200).json(deletedItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}
/* ------------------------------------------------------ */

/* ------------------------- PUT ------------------------ */
const editInstitution = async function (req, res, next) {
  const id = req.params.id
  try {
    const previousItem = await Institution.findById(id).exec()
    previousItem.name = req.body.name
    await previousItem.save()
    res.status(200).send(previousItem)
  } catch (e) {
    res.status(500).send({
      massage: e.massage
    })
  }
}

/* ------------------------------------------------------ */

/* ------------------------------------------------------ */
router.get('/:id', getInstitution)
router.get('/', getInstitutions)
router.post('/', addInstitution)
router.delete('/:id', deleteInstitution)
router.put('/:id', editInstitution)

/* ----------------------- TESTED ----------------------- */

module.exports = router
