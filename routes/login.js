// authen = คนนี้ใคร ?
const express = require('express')
const router = express.Router()
const User = require('../models/User')
const { generateAccessToken } = require('../middleware/generateToken')
const bcrypt = require('bcryptjs')

/* ---------------------- get by id --------------------- */
const login = async function (req, res, next) {
  // ต้อง getUser
  const username = req.body.username
  const password = req.body.password
  console.log(username, password)

  try {
    const user = await User.findOne({ username: username }).exec()
    console.log(user)

    const veifyResult = await bcrypt.compare(password, user.password)

    if (!veifyResult) {
      return res.status(404).json({ message: 'User not found' })
    }

    /* ------------------- generate token ------------------- */
    const token = generateAccessToken({ _id: user._id, username: user.username })
    console.log(token)
    /* ------------------- End generate token ------------------- */

    /* ------------------------ debug ----------------------- */
    res.status(200).json(
      {
        user: {
          _id: user._id,
          username: user.username,
          roles: user.roles
        },
        token: token
      })
  } catch (err) {
    return res.status(404).json({
      message: err.message + ' ..'
    })
  }
}

/* --------------------- add User -------------------- */
router.post('/', login)

module.exports = router
