const mongoose = require('mongoose')

const approverSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  approveres: {
    type: [mongoose.Schema.Types.ObjectId],
    required: true,
    default: [],
    ref: 'User'
  },
  institution: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Institution'
  }
})

/* ------------------------------------------------------ */

module.exports = mongoose.models.Approver || mongoose.model('Approver', approverSchema)
