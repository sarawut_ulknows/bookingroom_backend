
const mongoose = require('mongoose')

const bookingSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  datetime_reserve: {
    type: Date
  },
  room: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Room',
    required: true
  },
  datetime_start: {
    type: Date,
    required: true
  },
  datetime_end: {
    type: Date,
    required: true
  },
  status: {
    type: Number,
    default: 0
  },
  approves: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'Approve'
  }
})

module.exports = mongoose.model('Booking', bookingSchema)
