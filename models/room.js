const mongoose = require('mongoose')

const roomSchema = new mongoose.Schema({
  code: {
    type: String
  },
  capacity: {
    type: Number
    // required: true
  },
  floor: Number,
  building: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Building'
  },
  equipment: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'Equipment'
  },
  approveres: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Approver'
  },
  institution: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Institution'
  }
})

module.exports = mongoose.models.Room || mongoose.model('Room', roomSchema)
