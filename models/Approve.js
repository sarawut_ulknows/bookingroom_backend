const mongoose = require('mongoose')
const approveSchema = new mongoose.Schema({
  approver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  approve_date: {
    type: Date,
    default: null
  },
  approve_status: {
    type: Boolean,
    default: null
  }
})

module.exports = mongoose.model('Approve', approveSchema)
