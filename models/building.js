const mongoose = require('mongoose')

const buildingSchema = new mongoose.Schema({
  code: {
    type: String,
    required: true
  },
  name: String,
  floor: Number

})

module.exports = mongoose.model('Building', buildingSchema)
