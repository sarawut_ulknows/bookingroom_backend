/* ------------------- install package ------------------ */
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')
const mongoose = require('mongoose')
// const { ROLE } = require('./routes/ROLE')
/* ------------------------------------------------------ */

/* ------------------------ path ------------------------ */
const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
const institutionRouter = require('./routes/institutions')
const approveresRouter = require('./routes/approveres')
const equipmentRouter = require('./routes/equipments')
const buildingRouter = require('./routes/buildings')
const roomRouter = require('./routes/rooms')
const approveRouter = require('./routes/approves')
const bookingRouter = require('./routes/bookings')
const authenRouter = require('./routes/login')

/* ----------------------- helper ----------------------- */
// const { authenMiddleware } = require('./middleware/authen')
const dotenv = require('dotenv')
dotenv.config()
// const { authorizeMiddleware } = require('./middleware/author')

/* ------------------------------------------------------ */

/* ------------------------------------------------------ */

/* ------------------------- use ------------------------ */
const app = express()
app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
mongoose.connect('mongodb://localhost:27017/team')

/* ------------------------------------------------------ */

/* ---------------------- use path ---------------------- */
app.use('/', indexRouter)
// แสดงรายละเอียดห้อง
app.use('/users', usersRouter)
// จัดการหน่วยงาน --> Admin ใหญ่
app.use('/institutions', institutionRouter)
// แสดงรายละเอียดห้อง
app.use('/approveres', approveresRouter)
app.use('/equipments', equipmentRouter)
app.use('/buildings', buildingRouter)
// ค้นหาห้อง --> ได้ทุกคน แต่ดูรายละเอียดห้องไม่ได้
app.use('/rooms', roomRouter)
app.use('/approves', approveRouter)
app.use('/bookings', bookingRouter)
app.use('/login', authenRouter)

/* ------------------------------------------------------ */

/* ---------------------- port 3000 --------------------- */
// app.listen(3000, function () {
//   console.log('Server Listen at http://localhost:3000/')
// })
module.exports = app
